package com.example.parkcity.data.model;

import java.io.Serializable;

public class CarLocation implements Serializable {

    private double latitude;
    private double longitude;

    public CarLocation() {

    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
