package com.example.parkcity;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;

import com.example.parkcity.data.DatabaseRepository;
import com.example.parkcity.data.model.Parking;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.util.ArrayList;
import java.util.List;

public class ActivityRecognizedService extends IntentService {

    private static final int TIMES_UP_NOTIFICATION_ID = 145;
    private final String CHANNEL_ID = "NotificationChannel";
    private DatabaseRepository databaseRepository = new DatabaseRepository();

    public ActivityRecognizedService() {
        super("ActivityRecognizedService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (ActivityRecognitionResult.hasResult(intent)) {
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            handleDetectedActivity(result.getProbableActivities());
        }
    }

    private void handleDetectedActivity(List<DetectedActivity> probableActivities) {
        for (DetectedActivity activity : probableActivities) {
            switch (activity.getType()) {
                case DetectedActivity.IN_VEHICLE: {
                    if (activity.getConfidence() > 80 && isActiveParks()) {
                        promptUserToCancelParking();
                    }
                }
            }
        }
    }

    private boolean isActiveParks() {
        DatabaseReference dbRef = databaseRepository.getUserParkings();
        List<Parking> parks = new ArrayList<>();
        dbRef.get().addOnSuccessListener(new OnSuccessListener<DataSnapshot>() {
            @Override
            public void onSuccess(DataSnapshot dataSnapshot) {
                Parking p = dataSnapshot.getValue(Parking.class);
                parks.add(p);
            }
        });

        for (Parking p : parks) {
            if (p.isState()) {
                return true;
            }
        }

        return false;
    }

    private void promptUserToCancelParking() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Intent intentAction = new Intent(getApplicationContext(), FinishParkingReceiver.class);
        PendingIntent pendingIntentYesAction = PendingIntent.getBroadcast(getApplicationContext(), 1, intentAction, PendingIntent.FLAG_UPDATE_CURRENT);

        @SuppressLint("NotificationTrampoline") NotificationCompat.Builder builder = new NotificationCompat.Builder(ActivityRecognizedService.this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_baseline_access_time_filled)
                .setContentTitle(getString(R.string.finish_parking_notification_title))
                .addAction(R.drawable.ic_check_circle, "Yes", pendingIntentYesAction)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(TIMES_UP_NOTIFICATION_ID, builder.build());
    }
}
