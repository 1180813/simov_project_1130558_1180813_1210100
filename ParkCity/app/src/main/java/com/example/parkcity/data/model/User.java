package com.example.parkcity.data.model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Data class that captures user information for user in Firebase Database
 */

@IgnoreExtraProperties
public class User {

    public String name;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String name) {
        name = name;
    }
}
