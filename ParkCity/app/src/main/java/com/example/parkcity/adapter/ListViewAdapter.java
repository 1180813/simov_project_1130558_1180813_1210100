package com.example.parkcity.adapter;

import android.content.Context;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.parkcity.R;
import com.example.parkcity.data.model.Parking;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ListViewAdapter extends BaseAdapter {
    private final ArrayList<Parking> items;
    private FirebaseStorage storage;
    private StorageReference storageReference;


    public ListViewAdapter(ArrayList<Parking> items) {
        this.items = items;
        this.storage = FirebaseStorage.getInstance();
        this.storageReference = storage.getReference();
    }

    public int getCount() {
        return this.items.size();
    }

    public Object getItem(int arg0) {
        return this.items.get(arg0);
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public View getView(int arg0, View arg1, ViewGroup arg2) {
        // TODO Auto-generated method stub
        final Parking row = this.items.get(arg0);
        View itemView = null;

        if (arg1 == null) {
            LayoutInflater inflater = (LayoutInflater) arg2.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.list_view_park, null);
        } else {
            itemView = arg1;
        }

        if(row.isState()){
            itemView.setBackgroundColor(Color.argb(50,0, 100,34));
        }

        // Set the text of the row
        ImageView image = (ImageView) itemView.findViewById(R.id.parkingPhoto);
        String carPhoto = row.getCarPhoto();
        if(carPhoto!=null){
            StorageReference carPhotoRef = storageReference.child(carPhoto);
            carPhotoRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Picasso.get()
                            .load(uri)
                            .tag(ListViewAdapter.this)
                            .placeholder(R.drawable.ic_image)
                            .resize(100,100)
                            .into(image);
                }
            });
        }

        // Set the text of the row
        TextView parkingData = (TextView) itemView.findViewById(R.id.parkingData);
        if(row.getParkingDate()!=null){
            SimpleDateFormat df = new SimpleDateFormat("h:mm a dd/MM/yyyy", Locale.getDefault());
            String output = df.format(row.getParkingDate());
            parkingData.setText(output);
        }

        return itemView;
    }
}

