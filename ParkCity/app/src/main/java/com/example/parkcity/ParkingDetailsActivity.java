package com.example.parkcity;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.parkcity.data.DatabaseRepository;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class ParkingDetailsActivity extends FragmentActivity  {

    ImageView parkingImage;
    TextView parkingTimer;
    ImageView imageView;
    double currentLat = 0, currentLong = 0;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private Button stopButton;
    private boolean isActive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_details);

        setTitle("Parking Details");
        this.storage = FirebaseStorage.getInstance();
        this.storageReference = storage.getReference();
        Intent it = getIntent();
        Bundle params = it.getExtras();

        String parking_id = params.getString("Parking_id");
        String parking_date = params.getString("Parking_date");
        currentLat = params.getDouble("Parking_lat");
        currentLong = params.getDouble("Parking_long");
        isActive = params.getBoolean("Parking_active");
        String parking_photoURL = params.getString("Parking_photoURL");
        StorageReference carPhotoRef = storageReference.child(parking_photoURL);
        carPhotoRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.get().load(uri).into(parkingImage);
            }
        });

        parkingImage = findViewById(R.id.displayDetailsImageView);

        parkingTimer = findViewById(R.id.parkingTimer);

        stopButton = (Button) findViewById(R.id.stopButton);
        if (isActive == false) {
            stopButton.setVisibility(View.GONE);
        }

        stopButton.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatabaseRepository().endUserActiveParking();
                Intent intent = new Intent();
                intent.setClassName(getApplicationContext(), MainActivity.class.getName());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("remove_active_parking",true);
                getApplicationContext().startActivity(intent);
            }
        });

        parking_date = parking_date.replace("GMT+00:00 ", "");
        parkingTimer.setText(parking_date);

        imageView = findViewById(R.id.imageView);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction t = fm.beginTransaction();
        t.replace(R.id.google_maps_frame_layout, new GoogleMapsFragment(currentLat, currentLong));
        t.commit();

        QRGEncoder qrgEncoder = new QRGEncoder(parking_id, null, QRGContents.Type.TEXT, 500);
        try {
            Bitmap qrBits = qrgEncoder.getBitmap();
            imageView.setImageBitmap(qrBits);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}