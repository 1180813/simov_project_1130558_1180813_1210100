package com.example.parkcity;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.EditTextPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.example.parkcity.data.DatabaseRepository;
import com.example.parkcity.data.model.User;
import com.example.parkcity.ui.login.LoginActivity;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import androidx.preference.PreferenceManager;
import androidx.preference.PreferenceScreen;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.profile, new ProfileFragment())
                    .commit();
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public static class ProfileFragment extends PreferenceFragmentCompat implements PreferenceManager.OnPreferenceTreeClickListener {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_profile, rootKey);

            configureProfile();
            configureListeningForChanges();
        }

        private void configureListeningForChanges() {

            Preference examplePreference = findPreference("sign_out");

            examplePreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    FirebaseAuth.getInstance().signOut();
                    startActivity(new Intent(getContext(), LoginActivity.class));
                    return true;
                }
            });
        }

        private void configureProfile() {

            DatabaseRepository databaseRepository = new DatabaseRepository();

            DatabaseReference userDetails = databaseRepository.getCurrentUser();

            userDetails.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User userItem = dataSnapshot.getValue(User.class);

                    EditTextPreference namePreference = (EditTextPreference) findPreference("name");
                    EditTextPreference emailPreference = (EditTextPreference) findPreference("email");

                    namePreference.setText(userItem.name);
                    emailPreference.setText(databaseRepository.getCurrentUserEmail());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });

        }
    }
}