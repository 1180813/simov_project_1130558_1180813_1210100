package com.example.parkcity.data;

import android.location.Location;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.example.parkcity.data.model.CarLocation;
import com.example.parkcity.data.model.Parking;
import com.example.parkcity.data.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.sql.Date;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

public class DatabaseRepository {
    private final FirebaseAuth firebaseAuth;
    private DatabaseReference mDatabase;
    private FirebaseStorage storage;
    private StorageReference storageReference;

    public DatabaseRepository() {
        this.mDatabase = FirebaseDatabase.getInstance("https://parkcity-f997f-default-rtdb.europe-west1.firebasedatabase.app").getReference();
        this.firebaseAuth = FirebaseAuth.getInstance();
        this.storage = FirebaseStorage.getInstance();
        this.storageReference = storage.getReference();
    }

    // User
    public void writeNewUser(String authenticationID, String name) {
        User user = new User(name);

        mDatabase.child("users").child(authenticationID).setValue(user);
    }

    // Parking
    public String writeNewParking(Uri carPhoto, Location carLocation, boolean paidParking, boolean state) {

        // Set all other parking to not active, there can only be 1 active parking at a time
        endUserActiveParking();

        String carPhotoUID = UUID.randomUUID().toString();
        StorageReference ref = storageReference.child(carPhotoUID);
        ref.putFile(carPhoto);

        Parking p = new Parking();
        String parkingUID = mDatabase.child("estacionamento").push().getKey();
        p.setParkingID(parkingUID);
        p.setCarPhoto(carPhotoUID);
        p.setPaidParking(paidParking);
        p.setState(state);
        p.setParkingDate(Date.from(Instant.now()));

        CarLocation location = new CarLocation();
        location.setLatitude(carLocation.getLatitude());
        location.setLongitude(carLocation.getLongitude());
        p.setCarLocation(location);

        String uid = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
        if (p != null) {
            mDatabase.child("estacionamento").child(p.getParkingID()).setValue(p);
            mDatabase.child("user_parking").child(uid).child(parkingUID).setValue(parkingUID);
        }

        return parkingUID;
    }

    public DatabaseReference getCurrentUser() {

        String uid = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
        DatabaseReference userDetails = mDatabase.child("users").child(uid);
        userDetails.keepSynced(true);
        return userDetails;
    }

    public String getCurrentUserEmail() {
        return firebaseAuth.getCurrentUser().getEmail();
    }

    public DatabaseReference getCurrentUserName() {
        return getCurrentUser().child("name");
    }

    public DatabaseReference getUserParkings() {

        String uid = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
        DatabaseReference userDetails = mDatabase.child("user_parking").child(uid);
        userDetails.keepSynced(true);
        return userDetails;
    }

    public DatabaseReference getParking(String parkingId) {
        DatabaseReference userDetails = mDatabase.child("estacionamento").child(parkingId);
        userDetails.keepSynced(true);
        return userDetails;
    }

    public String getCurrentParkingURLById(String parkingId) {
        return mDatabase.child("estacionamento").child(parkingId).toString();
    }

    public void endUserActiveParking() {

        String uid = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

        DatabaseReference userParkings = getUserParkings();

        userParkings.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot userSnapshot: dataSnapshot.getChildren()){

                    String parkingID = userSnapshot.getKey();

                    mDatabase.child("estacionamento").child(parkingID).child("state").setValue(false);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println("error");
            }
        });
    }

    public void shareParking(String parkingUID) {
        String uid = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
        mDatabase.child("user_parking").child(uid).child(parkingUID).setValue(parkingUID);
    }
}
