package com.example.parkcity.data.model;

import android.location.Address;
import android.location.Location;
import android.net.Uri;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Random;

@IgnoreExtraProperties
public class Parking implements Serializable {

    private String parkingID;
    private String carPhoto;
    private CarLocation carLocation;
    private boolean paidParking;
    private boolean state;
    private Date parkingDate;
    public Parking() {

    }

    public String getParkingID() {
        return parkingID;
    }

    public void setParkingID(String parkingID) {
        this.parkingID = parkingID;
    }

    public String getCarPhoto() {
        return carPhoto;
    }

    public void setCarPhoto(String carPhoto) {
        this.carPhoto = carPhoto;
    }

    public CarLocation getCarLocation() {
        return carLocation;
    }

    public void setCarLocation(CarLocation carLocation) {
        this.carLocation = carLocation;
    }

    public boolean isPaidParking() {
        return paidParking;
    }

    public void setPaidParking(boolean paidParking) {
        this.paidParking = paidParking;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public Date getParkingDate() {
        return parkingDate;
    }

    public void setParkingDate(Date parkingDate) {
        this.parkingDate = parkingDate;
    }
}
